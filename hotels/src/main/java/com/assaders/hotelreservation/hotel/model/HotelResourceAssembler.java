package com.assaders.hotelreservation.hotel.model;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import com.assaders.hotelreservation.hotel.controller.ResourceController;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class HotelResourceAssembler implements ResourceAssembler<Hotel, Resource<Hotel>> {

    @Override
    public Resource<Hotel> toResource(Hotel hotel) {

        return new Resource<>(hotel,
                linkTo(methodOn(ResourceController.class).one(hotel.getId())).withSelfRel(),
                linkTo(methodOn(ResourceController.class).all()).withRel("hotel"));
    }
}
