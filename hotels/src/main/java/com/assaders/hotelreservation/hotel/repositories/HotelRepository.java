package com.assaders.hotelreservation.hotel.repositories;

import com.assaders.hotelreservation.hotel.model.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface HotelRepository extends JpaRepository<Hotel, UUID> {

}
