package com.assaders.hotelreservation.hotel.controller;

import com.assaders.hotelreservation.hotel.error.NotFoundException;
import com.assaders.hotelreservation.hotel.model.Hotel;
import com.assaders.hotelreservation.hotel.model.HotelResourceAssembler;
import com.assaders.hotelreservation.hotel.repositories.HotelRepository;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class ResourceController {

    private final HotelRepository repository;
    private final HotelResourceAssembler assembler;

    ResourceController(HotelRepository repository, HotelResourceAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    @GetMapping(value = "/hotel")
    public Resources<Resource<Hotel>> all() {
        List<Resource<Hotel>> employees = repository.findAll().stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<>(employees,
                linkTo(methodOn(ResourceController.class).all()).withSelfRel());
    }

    @GetMapping(value = "/hotel/{id}")
    public Resource<Hotel> one(@PathVariable UUID id) {
        Hotel hotel = repository.findById(id)
                .orElseThrow(() -> new NotFoundException(id));

        return assembler.toResource(hotel);
    }

    @PostMapping(value = "/hotel")
    public ResponseEntity<?> post(@RequestBody Hotel newHotel) throws URISyntaxException {
        Resource<Hotel> resource = assembler.toResource(repository.save(newHotel));

        return ResponseEntity
                .created(new URI(resource.getId().expand().getHref()))
                .body(resource);
    }

    @PutMapping(value = "/hotel/{id}")
    public ResponseEntity<?> put(@RequestBody Hotel newHotel, @PathVariable UUID id) throws URISyntaxException {
        Hotel updatedHotel = repository.findById(id)
                .map(hotel -> {
                    hotel.setName(newHotel.getName());
                    hotel.setDescription(newHotel.getDescription());
                    hotel.setAddress(newHotel.getAddress());
                    return repository.save(hotel);
                })
                .orElseGet(() -> {
                    newHotel.setId(id);
                    return repository.save(newHotel);
                });

        Resource<Hotel> resource = assembler.toResource(updatedHotel);

        return ResponseEntity
                .created(new URI(resource.getId().expand().getHref()))
                .body(resource);
    }

    @DeleteMapping(value = "/hotel/{id}")
    public ResponseEntity<?> delete(@PathVariable UUID id) {
        repository.deleteById(id);

        return ResponseEntity.noContent().build();
    }
}
