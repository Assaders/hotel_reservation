package com.assaders.hotelreservation.hotel.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
public class Hotel {
    @Id
    @GeneratedValue
    private UUID id;

    private String name;
    private String description;
    private String address;

    public Hotel(String name, String description, String address) {
        this.name = name;
        this.description = description;
        this.address = address;
    }
}
