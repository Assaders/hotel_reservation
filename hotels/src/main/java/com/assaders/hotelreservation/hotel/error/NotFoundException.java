package com.assaders.hotelreservation.hotel.error;

import java.util.UUID;

public class NotFoundException extends RuntimeException{

    public NotFoundException(UUID id) {
        super("Could not find hotel " + id);
    }
}
