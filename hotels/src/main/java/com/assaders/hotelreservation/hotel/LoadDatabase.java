package com.assaders.hotelreservation.hotel;

import com.assaders.hotelreservation.hotel.model.Hotel;
import com.assaders.hotelreservation.hotel.repositories.HotelRepository;
import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
class LoadDatabase {

    @Bean
    CommandLineRunner initDatabase(HotelRepository repository) {
        return args -> {
            log.info("Preloading " + repository.save(new Hotel("123", null, null)));
            log.info("Preloading " + repository.save(new Hotel("456", null, null)));
        };
    }
}
